﻿using System;
using System.Collections;

namespace lab8
{
	enum typeTrain { skTrain, pasTrain, tTrain };

	public interface ForSorting
	{
		void shell_sort(int[] arr);
	}

	class Programm {
		static int Sum(int x, int y)
		{
			return x + y;
		}
	}

	class MySortingClass : IComparable
	{
		private int val;
		public int setVal
		{
			set { val = value; }
			get { return val; }
		}



		public int CompareTo(object obj)
		{
			if (obj == null) return 1;

			MySortingClass smt = obj as MySortingClass;
			if (smt != null)
				return this.val.CompareTo(smt.val);
			else
				throw new ArgumentException("Object is not MySortingClass");
		}

	}

		class Element
		{
			public object val;
			public Element next;

			public Element(object o)
			{
				val = o;
			}

			public override string ToString()
			{
				return this.val.ToString();
			}

		}
		class MyList
		{
			public Element head;

			public MyList(Element h)
			{
				head = h;
			}

			public MyList() { }

			public void AddNew(Element h)
			{
				h.next = this.head;
				this.head = h;
			}

			public override string ToString()
			{
				string result = "";

				Element x = this.head;
				while (x.next != null)
				{
					result += x.ToString() + " ";
					x = x.next;
				}
				result += x.ToString();
				return result;
			}
		}



		class MainClass
		{
		
			public struct Train
			{
				public string number;
				public string message;
				public int cars;
				public typeTrain tTrain;

				public Train(string n, string m, int c, typeTrain t)
				{
					number = n;
					message = m;
					cars = c;
					tTrain = t;
				}

				public override string ToString()
				{
					return "number: " + number + "\nmessage: " + message + "\n# of cars: " + cars + "\ntype: " + tTrain;
				}

			}

			public static void Main(string[] args)
			{
				Train x = new Train("1234", "Hello", 20, typeTrain.skTrain);
				Console.WriteLine(x);

				// task #3. Value Types.
				SByte a = 0;
				Byte b = 0;
				Int16 c = 0;
				Int32 d = 0;
				Int64 e = 0;
				String s = "";
				Exception ex = new Exception();
				object[] types = { a, b, c, d, e, s };
				Console.WriteLine("Checking value types is true:");
				foreach (object o in types)
				{
					Console.WriteLine(o.GetType().IsValueType);
				}

				// task #4. Strings and arrays
				string sentence = "Bunny whore marshmellow kittens ufo";
				string[] pieces = sentence.Split(' ');
				Array.Sort(pieces);
				string sentence2 = String.Join(" ", pieces);
				Console.WriteLine("After split -> sort -> join:s " + sentence2);

				// task #4. part 2.
				Int16 n16 = 16;
				Int32 n32 = 32;
				double n_double = 13.65123;
				// convert int32 and int 16 to double

				Convert.ToInt32(n_double);
				Convert.ToInt32(n16);

				Convert.ToDouble(n32);
				Convert.ToDouble(n16);

				Convert.ToInt16(n32);
				Convert.ToInt16(n_double);

				// Task #5. Linked List
				MyList My = new MyList();
				Element one = new Element(1);
				Element two = new Element("Hoe");
				Element three = new Element(3);
				My.AddNew(one);
				My.AddNew(two);
				My.AddNew(three);
				Console.WriteLine(My.ToString());

			// Task #6. Interfaces
			ArrayList vector = new ArrayList();
			Random rnd = new Random();
			MySortingClass sample = new MySortingClass();
			sample.setVal = 50;

		      for (int ctr = 1; ctr <= 10; ctr++)
		      {
		         int random = rnd.Next(0, 100);
				MySortingClass temp = new MySortingClass();
				temp.setVal = random;
				vector.Add(temp);   
			 }

				     
			vector.Sort();
				
			}
		}
	}

