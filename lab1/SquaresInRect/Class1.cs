﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SquaresInRect
{
    public class Rect
    {
        int m, n;

        public Rect(int x, int y)
        {
            m = x;
            n = y;        
        }

        public int CountSquares()
        {
            if (m < n)
            {
                int z = m;
                m = n;
                n = z;
            }

            return m * (m + 1) * (2 * m + 1) / 6 + (n - m) * m * (m + 1) / 2;
        }
    }
}
