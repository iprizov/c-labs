﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Days
{
    public class SimpleCalendar
    {
        int day;
        int month;
        int year;

        public SimpleCalendar(int d, int m, int y)
        {
            if (m <= 12)
            {
                int DaysInMonth = System.DateTime.DaysInMonth(y, m);
                if (d <= DaysInMonth)
                {
                    day = d;
                    month = m;
                    year = y;
                }
            }
            else
            {
                month = 1;
                day = 1;
                year = 2017;
            }

        }

        public int DaysTillTheEndOfTheYear()
        {
            int daysPast = 0;

            for(int i = month; i != 0; i--)
            {
                if (i == month)
                {
                    daysPast += day;
                }
                else
                {
                    daysPast += System.DateTime.DaysInMonth(year, i);
                }
            }
            int daysInYear = System.DateTime.IsLeapYear(year) ? 366 : 365;
            int daysLeftInYear = daysInYear - daysPast ; // Result is in range 0-365.
            return daysLeftInYear;
        }

        public SimpleCalendar GetPrevDate()
        {
            int d = day - 1;
            int m, y;
            SimpleCalendar PrevDate;

            if (d > 0)
            {
                PrevDate = new SimpleCalendar(d, month, year);
            }
            else
            {
                if (month != 1)
                {
                    m = month - 1;
                    int daysInMonth = System.DateTime.DaysInMonth(year, m);
                    PrevDate = new SimpleCalendar(daysInMonth, m, year);
                }
                else
                {
                    m = 12;
                    y = year - 1;
                    d = 31;
                    PrevDate = new SimpleCalendar(d, m, y);
                }
            }
            return PrevDate;
        }

        public SimpleCalendar GetNextDate()
        {
            int d = day + 1, y, m;
            int daysInMonth = System.DateTime.DaysInMonth(year, month);
            SimpleCalendar NextDate;

            if (d <= daysInMonth)
            {
                NextDate = new SimpleCalendar(d, month, year);
            }
            else
            {
                if (month != 12)
                {
                    m = month + 1;
                    d = 1;
                    NextDate = new SimpleCalendar(d, m, year);
                }
                else
                {
                    y = year + 1;
                    m = 1;
                    d = 1;
                    NextDate = new SimpleCalendar(d, m, y);
                }
            }
            return NextDate;
        }

        public string GetDate()
        {
            string d, m;

            if (day < 10)
            {
                d = "0" + day.ToString();
            }
            else
            {
                d = day.ToString();
            }

            if (month < 10)
            {
                m = "0" + month.ToString();
            }
            else
            {
                m = month.ToString();
            }
            string date = d + ":" + m + ":" + year.ToString();
            return date;
        }


    }
}
