﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Chess
{
    public class Chessboard
    {
        int rows, cols;

        public Chessboard(int r, int c)
        {
            rows = r;
            cols = c;
        }

        public static bool IsOdd(int value)
        {
            return value % 2 != 0;
        }

        public int GetSize()
        {
            return rows * cols;
        }

        public string GetColor(int r, int c)
        {
	        string res = "fuck you";

            if (r*c <= rows * cols && r > 0 && c > 0)
            {
                if (IsOdd(r) && IsOdd(c) || !IsOdd(r) && IsOdd(c))
                {
                    res = "black";
                }

                if (IsOdd(r) && !IsOdd(c) || IsOdd(r) && !IsOdd(c))
                {
                    res = "white";
                }
		    return res;
            }

            return res;
        }

        public bool SameColor(int a1, int a2, int b1, int b2)
        {
            if (GetColor(a1, a2) == GetColor(b1, b2))
            {
                return true;
            }

            return false;
        }

        // pawn, knight, bishop, rook, queen

        public bool UnderAttack(int a1, int a2, int b1, int b2, string figure)
        {
            if (a1 <= rows && b1 <= rows && a2 <= cols && b2 <= cols && a1 > 0 && b1 > 0 && a2 > 0 && b2 > 0)
            {
                if (figure == "black_pawn")
                {
                    if (Math.Abs(a1 - b1) == Math.Abs(a2 - b2) && b1 - a1 == 1)
                    {
                        return true;
                    }
                    return false;

                }
                if (figure == "white_pawn")
                {
                    if (Math.Abs(a1 - b1) == Math.Abs(a2 - b2) && a1 - b1 == 1)
                    {
                        return true;
                    }
                    return false;
            
                }
                if (figure == "knight")
                {
                    if ((Math.Abs(a1 - b1 ) == 1 && Math.Abs(a2 - b2) == 2) || (Math.Abs(a1 - b1) == 2 && Math.Abs(a2 - b2) == 1))
                    {
                        return true;
                    }
                    return false;

                }
                if (figure == "bishop")
                {
                    if (Math.Abs(a1 - b1) == Math.Abs(a2 - b2))
                    {
                        return true;
                    }
                    return false;

                }
                if (figure == "rook")
                {
                    if ( a1 == b1 || a2 == b2)
                    {
                        return true;
                    }
                    return false;
                }
                if (figure == "queen")
                {
                    if ((a1 == b1 || a2 == b2) || (Math.Abs(a1 - b1) == Math.Abs(a2 - b2)))
                    {
                        return true;
                    }
                    return false;
               
                }
            }

            Console.WriteLine("Out of range...");
            return false;
        }
    }
}
