﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayTest
{
    class Arrs
    {
        private static Random rnd = new Random();

        public void CreateOneDimAr(int[] vector)
        {
            int m = 0;
            foreach(int i in vector)
            {
                vector[m] = rnd.Next(1, 100);
                m += 1;
            }
        }

        public int[,] CreateMultyDim(int a, int b)
        {
            int[,] c = new int[Math.Abs(a), Math.Abs(b)];
            return c;
        }

        public void FeedMatrix(int[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    matrix[i, j] = rnd.Next(1, 100);
                }
            }
        }

        public int[,] Multiply(int[,] a, int[,] b)
        {
            int[,] c = new int[a.GetLength(0), b.GetLength(1)];

            if (a.GetLength(1) == b.GetLength(0))
            {
                for (int i = 0; i< c.GetLength(0); i++)
                {
                    for (int j = 0; j < c.GetLength(1); j++)
                    {
                        c[i, j] = 0;
                        for (int k = 0; k < a.GetLength(1); k ++)
                        {
                            c[i, j] = c[i, j] + a[i, k] * b[k, j];
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("Unmultipliable");
            }
            return c;
        }

        public void PrintArr1(string name, int[] vector)
        {
            Console.Write(name + ": ");
            foreach (int i in vector)
            {
                Console.Write(i + " ");
            }
            Console.WriteLine();
        }

        public void PrintArr2(string name, int[,] matrix)
        {
            int i, j;
            Console.WriteLine(name + ":");
            for (i = 0; i < matrix.GetLength(0); i++)
            {
                for (j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(matrix[i, j] + " ");
                }
                Console.WriteLine();
            }

        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Arrs bucky = new Arrs();
            int[] aa = new int[5],
                bb = new int[5],
                cc = new int[5],
                XXX = { 1, 2, 3, 4, 5, 6, 6 },
                U = new int[3], V = new int[3];

            int[,] AAA = new int[3, 4]
            {
                {0,1,2,4 },
                {2,3,4,5 },
                {2,3,4,5 },
            };
            int[,] BBB, CCC;
            BBB = bucky.CreateMultyDim(4, 3);
            Console.WriteLine(AAA.GetLength(1));
            Console.WriteLine("---------------Matrices--------------");

            bucky.PrintArr2("BBB: ", BBB);
            bucky.FeedMatrix(BBB);
            CCC = bucky.Multiply(AAA, BBB);
            bucky.PrintArr2("CCC: ", CCC);
            bucky.PrintArr2("BBB: ", BBB);
            bucky.PrintArr2("matrix: ", AAA);
            Console.WriteLine();

            bucky.CreateOneDimAr(aa);
            bucky.CreateOneDimAr(bb);

            for(int iii = 0; iii < cc.Length; iii+=1)
            {
                cc[iii] = aa[iii] + bb[iii];
            }

            for (int iii = 0; iii < 3; iii += 1)
            {
                U[iii] = iii + 1;
            }

            V = U;

            bucky.PrintArr1("V = U ", V);

            bucky.PrintArr1("U with 3 items only: ", U);
            bucky.PrintArr1("A", aa);
            bucky.PrintArr1("B", bb);
            bucky.PrintArr1("Sum of A & B:", cc);

        }
    }
}
