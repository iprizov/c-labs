﻿using System;

namespace lab7
{

	class Person
	{
		private string _name;
		private string _status;
		private string _health;
		private double _salary;
		private int _age;

		public Person(string n, string s, string h, double salary, int a)
		{
			_name = n;
			_status = s;
			_health = h;
			_salary = salary;
			_age = a;

			Console.WriteLine("Object {0} of Class Person created successfully", _name);
		}


		public string Name 
		{ 
			set { if (_name == "") _name = value; }
			get { return _name;}
		}
		public string Status 
		{ 
			get { return _status; }
		}
		public double Salary 
		{
			set { if (value > 0) _salary = value; }
		}
		public int Age {
			get { return _age; }
			set { if (value > 0 && value < 100) _age = value; }
		}
		public string GetHealthStatus() 
		{
			return this._name + " has a " + this._health + " health";
		}
	}

	class Rational
	{
		public static readonly int one;
		public static readonly int zero;
		
		private int m;
		private int n;
		private int gcd;

		static Rational()
		{
			zero = 0;
			one = 1;
		}

		public Rational(int a, int b)
		{
			if (b == 0)
			{
				m = 0;
				b = 1;
			}
			else
			{ 
				gcd = GCD(a, b);
				m = a/gcd;
				n = b/gcd;
			}

		}

		private Rational(int a, int b, string t)
		{
			m = a; n = b;	
		}

		private int GCD(int a, int b)
		{
			while (a != 0 && b != 0)
			{
				if (a > b)
					a %= b;
				else
					b %= a;
			}

			return a == 0 ? b : a;
		}

		public override string ToString()
		{
			return m.ToString() + "/" + n.ToString();
		}

		public System.Reflection.FieldInfo[] GetInfo()
		{
			var fields = this.GetType().GetFields(System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
			return fields;
		}

		// # 3 overloading + - * /

		public static Rational operator +(Rational a, Rational b)
		{
			int den = a.GCD(a.n, b.n);
			den = (a.n * b.n) / den;
			int num = a.m * (den / a.n) + b.m * (den / b.n);
			Rational result = new Rational(num, den);
			return result;
		}

		public static Rational operator -(Rational a, Rational b)
		{
			int den = a.GCD(a.n, b.n);
			den = (a.n* b.n) / den;
			int num = a.m * (den / a.n) - b.m * (den / b.n);
			Rational result = new Rational(num, den);
			return result;
		}

		public static Rational operator *(Rational a, Rational b)
		{
			int den = a.n * b.n;
			int num = a.m * b.m;
			Rational result = new Rational(num, den);
			return result;
		}

		public static Rational operator /(Rational a, Rational b)
		{
			int den = a.n * b.m;
			int num = a.m * b.n;
			Rational result = new Rational(num, den);
			return result;
		}

		// #4 overloading != == < >

		public static bool operator ==(Rational a, Rational b)
		{
			bool status = false;
			if (a.m == b.m && a.n == b.n)
				status = true;
			return status; 
		}

		public static bool operator !=(Rational a, Rational b)
		{
			bool status = false;
			if (a.m != b.m || a.n != b.n)
				status = true;
			return status; 
		}

		public static bool  operator >(Rational a, Rational b)
		{
			return Convert.ToDouble(a.m) / Convert.ToDouble(a.n) > Convert.ToDouble(b.m) / Convert.ToDouble(b.n);
		}
		public static bool operator >(Rational a, double b)
		{
			return Convert.ToDouble(a.m) / Convert.ToDouble(a.n) > b;
		}

		public static bool operator >(double a, Rational b)
		{
			return a> Convert.ToDouble(b.m) / Convert.ToDouble(b.n);
		}

		public static bool operator <(Rational a, Rational b)
		{
			return Convert.ToDouble(a.m) / Convert.ToDouble(a.n) < Convert.ToDouble(b.m) / Convert.ToDouble(b.n);
		}
		public static bool operator <(Rational a, double b)
		{
			return Convert.ToDouble(a.m) / Convert.ToDouble(a.n) < b;
		}

		public static bool operator <(double a, Rational b)
		{
			return a < Convert.ToDouble(b.m) / Convert.ToDouble(b.n);
		}


	}

	class MainClass
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("lab #6 Classes!");

			// Task #1. Make class Person with properties 
			Person Jim = new Person("Jim", "The King", "bad", 35000, 45);
			Console.WriteLine(Jim.GetHealthStatus());
			Console.WriteLine("{0}'s age is {1} ", Jim.Name, Jim.Age);
			Jim.Age = 46;
			Console.WriteLine("{0} had a birthday! Now his age is {1} ", Jim.Name, Jim.Age);
			Console.WriteLine("His job position is - {0}", Jim.Status);

			// Task #2. Make class Rational
			Rational x = new Rational(8, 4);
			Console.WriteLine("The number is: {0}", x.ToString());

			System.Reflection.FieldInfo[] data = x.GetInfo();
			Console.WriteLine("Class structure: ");
			foreach (var i in data)
			{
				Console.WriteLine(i);
			}

			// Task #3. Overloading + - * /
			Rational a = new Rational(1, 2);
			Rational b = new Rational(1, 100);
			Rational c = a + b;
			Console.WriteLine(c.ToString());

			// Task #4. Static constructor & overloading == != < >
			Console.WriteLine("1/2 > 1/100 ? {0} ", a < b);

			Console.WriteLine(Rational.one);
			Console.WriteLine(Rational.zero);

		}
	}
	
}
