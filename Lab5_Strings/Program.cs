﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5_Strings
{
    class Program
    {

        public static string ReverseString(string s)
        {
            char[] arr = s.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }

        public static void PrintCharArray(char[] arr)
        {
            foreach(char c in arr)
            {
                Console.WriteLine(c);
            }
        }
        public static String CharArrayToString(char[] ar)
        {
            String result = new string(ar);
            return result;
        }
        public static void TestStringBuilder()
        {
            StringBuilder str1 = new StringBuilder("String - example of StringBuilder"),
                str2 = new StringBuilder("String - example 2 of StringBuilder");
            str1.Append(" hello");
            Console.WriteLine(str1);
            Console.WriteLine(str2);

            while (true)
            {
                if (str1 == str2)
                {
                    Console.WriteLine("Strings are equal");
                }
                else
                {
                    str2 = str1;
                    Console.WriteLine("Strings are equal:");
                    Console.WriteLine(str1);
                    Console.WriteLine(str2);
                    break;
                }
            }

            
        }
        public static List<int> TestIndexSym(char[] s1, char[] s2)
        {
            int i, j, z = 0;
            List<int> vector = new List<int>(); ;

            if (s2.Length <= s1.Length)
            {
                for (i = 0; i < s1.Length; i++)
                {
                    if (s1[i] == s2[0])
                    { 
                        for (j = 0; j < s2.Length; j++)
                        {
                            if (s1[i++] == s2[j])
                            {
                                z += 1;
                            }
                        }
                    }
                     if (z == s2.Length)
                     {
                         vector.Add(i - z);
                        z = 0;
                     }


                }
            }
            return vector;
        }
        public static int IndexOfString(char[] s1, char[] s2)
        {
            int i, j, z = 0;

            if (s2.Length <= s1.Length)
            {
                for(i = 0; i < s1.Length; i++)
                {
                    if (s1[i] == s2[0])
                    {
                        for (j = 0; j < s2.Length; j++)
                        {
                            if (s1[i++].Equals(s2[j]))
                            {
                                z += 1;
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("Z: " + z);
                        Console.WriteLine("s1 len: " + s1.Length);
                        Console.WriteLine("s2 len: " + s2.Length);
                        if (z == s2.Length)
                        {
                            Console.WriteLine("DOne!");
                            return i - z -1;
                        }
                        z = 0;
                    }

                }
            }
            return s1.Length - z -1 ;
        }
        public static void SplitJoin(string txt)
        {
            string[] Sentences, Words;
            string Echo;

            Sentences = txt.Split(',');
            Words = txt.Split(' ');

            Echo = String.Join(" ", Words);
            Console.Write("With Join: ");
            Console.WriteLine(Echo);
            Console.Write("Original: ");
            Console.WriteLine(txt);
        }

        public static string SumInWords(int numb)
        {
            int i = 0, z;
            string currency = "", for_processing;
            string[] numbers =
            {
                "один", "два",
                "три", "четыре", "пять",
                "шесть", "семь", "восемь",
                "девять"
            };
            string[] tricky =
            {
                "одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемьнадцать", "девятнадцать"
            };
            string[] decimals =
            {
                "десять", "двадцать", "тридцать", "сорок", "пятдесят", "шестдесят", "семдесят", "восемьдесят", "девяносто"
            };
            string[] hundreds =
            {
                "сто", "двести", "триста", "четыреста", "пятсот", "шестос", "семьсот", "восемьсот", "девятсот"
            };
            string[] thouthands =
            {
                "тысяча", "тысячи две", "тысячи три", "тысячи четыре", "тысяч пять", "тысяч шесть", "тысяч семь", "тысяч восемь", "тысяч девять"
            };

            for_processing = ReverseString(numb.ToString());

            foreach(char c in for_processing)
            {

                z = (int)Char.GetNumericValue(c);
                i += 1;
                switch(i)
                {
                    case 1:
                        if (z > 0)
                        {
                            currency = numbers[z - 1];
                        }
                        break;
                    case 2:
                        int first = (int)Char.GetNumericValue(for_processing[0]); ;

                        if (c != '0')
                        {
                            if (c == '1' && for_processing[0] != '0')
                            {
                                currency = tricky[first - 1];
                            }
                            else
                            {
                                string tens = decimals[z - 1];
                                if (first - 1 >= 0)
                                {
                                    string base_1 = numbers[first - 1];
                                    currency = base_1;
                                }
                                currency += " " + tens;
                            }
                        }
                        else
                        {
                            if (first != 0)
                            {
                                currency = numbers[first - 1];
                            }
                        }
                        break;
                    case 3:
                        if (z - 1 >= 0)
                        {
                            currency += " " + hundreds[z - 1];
                        }
                        break;
                    case 4:
                        currency += " " + thouthands[z - 1];
                        break;
                }

            }
            var words = currency.Split(' ');
            var res = words.Reverse();
            currency = String.Join(" ", res);

            int conjugation = numb % 100;
            if (numb % 10 >4 && numb %10 <= 19 || numb %10 == 0)
            {
                currency += " рублей";
            }
            else
            {
                if (numb % 10 == 1)
                {
                    currency += " рубль";
                }
                else
                {
                    currency += " рубля";
                }
            }
            Console.WriteLine(currency);
            return currency;
        }


        static void Main(string[] args)
        {
            string s = "test";
            string str2 = new string('s', 5);
            Console.WriteLine(str2+s);

            // next one
            SumInWords(1234);
            SumInWords(5000);
            SumInWords(1031);
            SumInWords(7401);
            SumInWords(1);
            SumInWords(29);
            SumInWords(9);
            SumInWords(741);
            SumInWords(12341);

            // SplitJoin
            String story = "Bernard the bear, the coolest bear ever, he loves pizza";
            SplitJoin(story);

            //StringBuilder

            TestStringBuilder();

            StringBuilder str1 = new StringBuilder();
            StringBuilder hello = new StringBuilder("hello");

            int num = 1;
            String[] chunks = story.Split(',');
            foreach(string str in chunks)
            {
                str1.AppendFormat("{0}: {1}", num++, str);               
                Console.WriteLine(str1);
            }

            Console.WriteLine(str1.Capacity);
            Console.WriteLine(str1.MaxCapacity);
            str1.EnsureCapacity(100);
            Console.WriteLine(str1.Capacity);
            Console.WriteLine(str1.MaxCapacity);
            Console.WriteLine(hello.Capacity);

            char[] thisChar = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'A', 'D', 'C' };
            char[] abc = { 'D', 'E', 'F' };
            char[] d = { 'D' };
            string STR2 = CharArrayToString(thisChar);

            Console.WriteLine("This is char array outputed with a loop 'foreach':");
            PrintCharArray(thisChar);
            Console.WriteLine("This is a char array converted to String: "+STR2);

            Console.WriteLine("Position of  d e f: " + IndexOfString(thisChar, abc));
            TestIndexSym(thisChar, d).ForEach(i => Console.Write("{0} ", i));
        }
    }
}
