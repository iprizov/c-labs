﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ComplexNumbers;
using Chess;
using Days;
using Armstrong;
using SquaresInRect;

namespace lab2
{

    class Program
    {
        static void Main(string[] args)
        {

            Complex a = new Complex(12, 1);
            Complex b = new Complex(1, 4);
            Complex c;

            c = Complex.sum(a, b);
            Console.WriteLine(a.ToString());
            Console.WriteLine(b.ToString());
            Console.WriteLine(c.ToString());


            // Part 2
            Console.WriteLine("Output Mask:");
            int i = 3, j = 4;
            Console.WriteLine("i:{0} j:{1}", i, j);
            Console.WriteLine("++i:{0} --j:{1}", ++i, --j);
            Console.WriteLine("i++:{0} j--:{1}", i++, j--);
            Console.WriteLine("i:{0} j:{1}", i, j);

            Console.WriteLine("The Negation:");
            bool A = true, B = false;
            Console.WriteLine("-i:{0} -j:{1}", -i, -j);
            Console.WriteLine("!A:{0} !B:{1}", !A, !B);

            // Chess game
            Console.WriteLine("-----------------Chess Game-----------------");
            Chessboard board = new Chessboard(8, 8);
            Console.Write("Board size: ");
            Console.WriteLine(board.GetSize());


            string color = board.GetColor(5, 5);
            Console.WriteLine("Color of (5, 5) is " + color);

            Console.WriteLine("Colors of (5, 5) and (3, 3) same? " + board.SameColor(5, 5, 3, 3));
            Console.WriteLine("Colors of (5, 5) and (5, 6) same? " + board.SameColor(5, 5, 5, 6));

            Console.WriteLine("\n\n\n-----------------Figures:-----------------");
            Console.WriteLine("Black pawn at (5, 5) another figure at (6, 6): " + board.UnderAttack(5, 5, 6, 6, "black_pawn"));
            Console.WriteLine("White pawn at (5, 5) another figure at (6, 6):" + board.UnderAttack(5, 5, 6, 6, "white_pawn"));

            Console.WriteLine("Bishop at (1, 1) another figure at (3, 3): " + board.UnderAttack(1, 1, 3, 3, "bishop"));
            Console.WriteLine("Bishop at (1, 1) another figure at (1, 2): " + board.UnderAttack(1, 1, 1, 2, "bishop"));

            Console.WriteLine("Queen at (3, 4) another figure at (1, 8): " + board.UnderAttack(3, 4, 1, 8, "queen"));
            Console.WriteLine("Queen at (5, 4) another figure at (5, 2): " + board.UnderAttack(5, 4, 5, 2, "queen"));
            Console.WriteLine("Queen at (2, 2) another figure at (4, 4): " + board.UnderAttack(2, 2, 4, 4, "queen"));

            Console.WriteLine("Rook at (5, 8) another figure at (5, 1):" + board.UnderAttack(5, 8, 5, 1, "rook"));

            // days

            SimpleCalendar d1 = new SimpleCalendar(1, 12, 2017), d2, d3;
            int left;
            Console.WriteLine("\n\n\n-----------------Days:-----------------");

            left = d1.DaysTillTheEndOfTheYear();
            d2 = d1.GetNextDate();
            d3 = d1.GetPrevDate();

            Console.WriteLine("Today: "+d1.GetDate());
            Console.WriteLine("Tomorrow: " + d2.GetDate());
            Console.WriteLine("Yesterday: " + d3.GetDate());
            Console.WriteLine("Till the end of the year left " + left + " days");

            Console.WriteLine("\n\n\n-----------------Amstrong Numbers-----------------");
            Numbers armstr = new Numbers();
            for(int y = 100; y < 1000; y++)
            {
                if (armstr.isArmstrong(y))
                {
                    Console.WriteLine(y);
                }
            }

            Console.WriteLine("\n\n\n-----------------Squares in a Rect-----------------");

            Rect suzy = new Rect(3, 4);
            int squares = suzy.CountSquares();
            Console.WriteLine("Amount of Squares in this rectangle:" + squares);



        }
    }
}
