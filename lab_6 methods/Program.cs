﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_6_methods
{
    class Program
    {
        static void Main(string[] args)
        {
            double a=2.2, b=8;
            Example.Sqr(a, b);
            Example.Sqr(a * 2, b * 2);
            Console.WriteLine("In method Main: a = {0}\tb = {1}", a, b);

            Example boo = new lab_6_methods.Example(3);
            Console.WriteLine("In boo x = {0}", boo.x);
            Console.WriteLine("And now:");
            Example.Init(boo);
            Console.WriteLine("In boo x = {0}, y = {1}", boo.x, boo.y);

            Example y = Example.NewInit();
            Console.WriteLine("In Y x = {0}, y = {1}", y.x, y.y);

            Console.WriteLine("----------Methods with ref----------");
            Example.Sqr_r(ref a, ref b);

            Console.WriteLine("In method main Main: a = {0}\tb = {1}", a, b);
            Example.Init_r(ref boo);
            Console.WriteLine("In boo x = {0}, y = {1}", boo.x, boo.y);

            // #3
            //(double x, out Boolean even, out int sign, out double square, out double abs)

            double abs, square;
            int sign;
            bool even;
            double hello = 5.3, goobye = -4.0;
            Example.GetData(hello, out even, out sign, out square, out abs);
            Console.WriteLine("Is Even: " + even);
            Console.WriteLine("Sign: " + sign);
            Console.WriteLine("Square: " + square);
            Console.WriteLine("Abs: " + abs);

            Example.GetData(goobye, out even, out sign, out square, out abs);
            Console.WriteLine("Is Even: " + even);
            Console.WriteLine("Sign: " + sign);
            Console.WriteLine("Square: " + square);
            Console.WriteLine("Abs: " + abs);

            // #4
            int[] vector = { 1, 2, 3, 4 };
            double av = Example.Avg(vector);
            Console.WriteLine("Avg: " + av);

            short z = 1, e = 12;
            byte v = 107;
            Console.WriteLine("Avg: " + Example.Avg(z, e, v));
            Console.WriteLine("Avg hey: " + Example.Avg());

            // #5
            int[] vector2 = { 1, 2, 33, 4 };
            Example.Params(vector);
            Console.WriteLine("Vector2:");
            foreach(int j in vector2)
            {
                Console.WriteLine(j);
            }

            string quote = "The dog always barks";
            Console.WriteLine("String before it was passed to the method: " + quote);
            Example.AlterString(quote);
            Console.WriteLine("String after it was passed to the method: " + quote);
            Example.AlterString_r(ref quote);
            Console.WriteLine("String after it was passed to the method - Example.AlterString_r(ref quote): " + quote);

            Example.ConsoleType(1.2);
        }
    }
    class Example
    {
        public int x, y;

        public Example(int x)
        {
            this.x = x;
        }
        public static void Init(Example eg)
        {
            eg.x = 1;
            eg.y = 1;
            Console.WriteLine("x = {0}\ny = {1}", eg.x, eg.y);
        }
        public static Example NewInit()
        {
            Example x = new Example(1);
            x.y = 1;
            return x;
        }

        public static void Sqr(double a, double b)
        {
            a = Math.Pow(a, 2);
            b = Math.Pow(b, 2);
            Console.WriteLine("In method Sqr. a = {0}\tb = {1}", a, b);
        }
        // Methods where params are passed by reference 
        public static void Sqr_r(ref double a, ref double b)
        {
            a = Math.Pow(a, 2);
            b = Math.Pow(b, 2);
            Console.WriteLine("In method Sqr. a = {0}\tb = {1}", a, b);
        }

        public static void Init_r(ref Example eg)
        {
            eg.x = 1;
            eg.y = 1;
            Console.WriteLine("x = {0}\ny = {1}", eg.x, eg.y);
        }
        // #3 Get all data from a var 
        public static void GetData(double x, out Boolean even, out int sign, out double square, out double abs)
        {
            square = Math.Pow(x, 2);
            abs = Math.Abs(x);
            even = x % 1 == 0 ? true : false;
            sign = Math.Sign(x);
        }
        // #4
        public static double Avg(params int[] vector)
        {
            double avg = 0;

            foreach (int i in vector)
            {
                avg += i;
            }
            return avg/vector.Length;
        }
        // #5
        public static void Params(params int[] vector)
        {
            for(int z = 0; z < vector.Length; z++)
            {
                vector[z] *= 2;
            }
        }
        public static void AlterString(string z)
        {
            z += " (c)";
        }
        public static void AlterString_r(ref string z)
        {
            z += " (c)";
        }
        public static void ConsoleType(params Object[] a)
        {
            Console.WriteLine(a.GetType());
        }
    }
}
